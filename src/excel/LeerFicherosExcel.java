package excel;

import java.io.*;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class LeerFicherosExcel {
    public static void main(String[] args) throws IOException {
        Scanner entrada = new Scanner(System.in);
        String vain;
        String tipocuen;

        System.out.println("Ingrese I para una cuenta invalida o V para valida");
        vain = entrada.nextLine();
        System.out.println("Ingrese el tipo de cuenta");
        tipocuen = entrada.nextLine();
        
        readXLSFile(vain, tipocuen);
    }

    public static void readXLSFile(String entrada, String tipocuen) throws IOException {
        InputStream ExcelFileToRead = new FileInputStream("C:/Users/SIES/Desktop/Salma/Excel/Libro4.xls");
        HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow row;
        HSSFCell cell;

        Iterator rows = sheet.rowIterator();

        while (rows.hasNext()) {
            row = (HSSFRow) rows.next();
            Iterator cells = row.cellIterator();
            System.out.print("INSERT INTO SIES_CUENTASPRUEBA (INSTITUCION, CUENTA, TIPO, BENEFICIARIO, RFC, TIPO_CUENTA) VALUES (");

            while (cells.hasNext()) {
                cell = (HSSFCell) cells.next();

                if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                    if (cell.getColumnIndex() == 2) {
                        System.out.print(", '" + entrada + "'");
                    }
                    System.out.print(", '" + cell.getStringCellValue() + "'");
                } else if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                    System.out.print((int) cell.getNumericCellValue() + "");
                }
              
            }
            if (rows.hasNext()) {
                System.out.print(", " + tipocuen + "),");
                System.out.println();
            } else {
                System.out.print(", " + tipocuen + ");");
                System.out.println();
            }
        }
    }
}
